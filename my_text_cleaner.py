import nltk
from nltk.stem.snowball import FrenchStemmer
import sys
import json
import spacy
from spellchecker import SpellChecker
import re
from nltk.corpus import wordnet as WN
from nltk.corpus import stopwords


class CleanComments:
    """
    Object that cleanup a json file of comment by applying
    lemmatizer and spelling mistakes checker
    """

    def __init__(self, dic_comments_file_path, cleaned_file_path="data/cleaned.json"):
        """
        :param dic_comments_file_path: The file path that holds the comment in json format
         uid: {'comment' : "message"}, ...
         :param cleaned_file_path is the file's location where the translated data will be stored
        """
        # define emoji pattern to delete unwanted emoji
        self.emoji_pattern = re.compile("["
                                   u"\U0001F600-\U0001F64F"  # emoticons
                                   u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                                   u"\U0001F680-\U0001F6FF"  # transport & map symbols
                                   u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                                   "]+", flags=re.UNICODE)
        self.unwanted_punc = ('!', '.', ':', ',')
        self.cleaned_data_dic = {}
        self.cleaned_file_path = cleaned_file_path
        self.dic_comments = json.loads(self._read_file(dic_comments_file_path),)
        # Lemmaniz using --> https://spacy.io/models/fr
        # based on french language, core: Vocabulary, syntax, entities, vectors,
        self.nlp = spacy.load('fr_core_news_md')
        #self.stop_words_en = set(stopwords.words('french'))
        self.spell = SpellChecker()
        self.spell.word_frequency.load_text_file('data/dictionary.txt')

    @staticmethod
    def _read_file(file):
        with open(file) as reader:
            return reader.read()

    def _save_file(self):
        with open(self.cleaned_file_path, 'w') as cleaned_file:
            return cleaned_file.write(json.dumps(self.cleaned_data_dic, indent=2))

    def remove_unwanted_punct(self, string):
        return "".join(c for c in string if c not in self.unwanted_punc)

    def remove_emoji(self, text):
        return self.emoji_pattern.sub(r'', text)

    @staticmethod
    def _tokens(sent):
        """ Use nltk tokenizer to split words from a given text """
        return nltk.word_tokenize(sent, language='french')

    def spell_checker(self, tokens):
        new_token = ''
        for word in tokens:
            # Spell checking
            if len(word) >= 10:
                # set distance of 1 of the Levenshtein Distance for longer words
                self.spell.distance = 1
            token = self.spell.correction(word)
            print('spell check: {} ------'.format(token))
            new_token += ' ' + token
            # set distance 2 (default)
            self.spell.distance = 2
        return new_token

    def spellcheck(self, tokens):
        new_token = ''
        for word in tokens:
            strip = word.strip()
            new_token += ' ' + self.spell_check.spell_check(strip)
        return new_token

    def lemmatize(self, checked_spell_text):
        doc = self.nlp(checked_spell_text)
        lemmatized_text = ''
        for token in doc:
            lemmatized_text += ' ' + token.lemma_
        return lemmatized_text

    def clean_up_data(self):
        """
            Loop over all the message contained into the provided parameter
            then apply cleaning process:
            Removing unwanted punctuations
            Removing emojis
            Tokenizing the sentence with french corpus words
            Checking spelling mistakes using SpellChecker with a custom dictionary (based on Levenshtein Distance alg.)
            Lemmanize the remaing
        """
        for key, comment in self.dic_comments.items():
            text = comment['comment']
            # remove useless punctuation
            text = self.remove_unwanted_punct(text)
            # remove emojis
            text = self.remove_emoji(text)
            # tokenize the sentence
            tokens = self._tokens(text)
            # check spelling
            checked_spell_text = self.spell_checker(tokens)
            lemmatized_text = self.lemmatize(checked_spell_text)
            print(lemmatized_text)
            # delete the original text
            del comment['comment']
            comment['comment'] = lemmatized_text.strip()
            self.cleaned_data_dic[key] = comment
        self._save_file()


if __name__ == '__main__':
    if len(sys.argv) < 1:
        print("Usage: {} FILE_PATH".format(sys.argv[0]))
        sys.exit(1)
    data_file = sys.argv[1]
    cleaner = CleanComments(data_file)
    cleaner.clean_up_data()
