# How to Setup ?

pip3 install spacy
python3 -m spacy download fr_core_news_md

pip3 install nltk

pip3 install pyspellchecker


# How to Run ?

python my_text_cleaner.py name_dico_file

## Data

https://raw.githubusercontent.com/hbenbel/French-Dictionary/master/dictionary/
